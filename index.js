const contentful = require('contentful');
const fs = require('fs');
const express = require('express');
const app = express();
const Liquid = require('liquid-node');
const engine = new Liquid.Engine;
const http = require('http');
const fetch = require('node-fetch');

const productionKey = 'c658e6c41b6e2db78f166fa28e61f58b81e781dc227258c828bf0d700421d004';
const spaceId = '0gi6y2uhh5wr';
engine.registerFileSystem(new Liquid.LocalFileSystem("./views"));

var urlToRequest = 'http://cdn.contentful.com/spaces/{{spaceId}}/entries/{{entryId}}?access_token={{productionKey}}'

app.get('/:routeName/:location/:id', function (req, res) {
  processFile(req.params.routeName, req.params.id, function (err, result) {
    if (err) { // Did something go wrong
      console.log('error',err);
      return res.send('error');
    }
    return res.send(result);
  });
});

app.listen(3000, function () {
  console.log('Listening on port 3000 - test url http://localhost:3000/local-pages/anywhereintheworld/MHa7Y8UeiGmK8Wc8YgikO');
});

function processFile(routeName, id, callback){
  var folderPath = './files/' + routeName;

  // Read the contents of the file
  let filePath = folderPath + '/index.html';
  fs.readFile(filePath, 'utf8', function (err, data) {
    if (err) { return callback(err); }
    processEntry(id, data).then(callback.bind(null, null));   // callback(null, result)
  });

}

function processEntry(entryId, data) {
  let pathToRequest = urlToRequest.replace('{{spaceId}}', spaceId);
  pathToRequest = pathToRequest.replace('{{productionKey}}', productionKey);
  pathToRequest = pathToRequest.replace('{{entryId}}', entryId);

  return fetch(pathToRequest)
    .then(function(res) {
        return res.json();
    }).then(function(json) {
      // Replace any liquid variables in the HTML file
      return engine
      .parse(data)
      .then(function(template) { return template.render(json.fields); });
    });
}
